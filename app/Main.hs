{-# LANGUAGE BangPatterns #-}
module Main where
import qualified Data.Map as Map
import Data.List (sort,group)
import Data.Function ((&))

children :: (Foldable t,Ord k,Ord b) => t k -> Map.Map k [b] -> [b]
children p g = concatMap (g Map.!) p & sort & group & map head

bfs :: (Ord k) =>  k -> Map.Map k [k] -> [k]
bfs s g = go' [] [s] where
  go' v [] = v
  go' v !q = go' (v++q) (filter(\x -> notElem x (v++q)) (children q g))

main :: IO ()
main = do
  let graph = Map.fromList [(0,[1,2]),(1,[0,3,4]),(2,[0,1]),(3,[1,5]),(4,[1])
                     ,(5,[3,6,7,8]),(6,[5]),(7,[5,8]),(8,[5,7,9]),(9,[8])]
  print(bfs 0 graph)
